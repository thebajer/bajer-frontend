# BAJer-frontend

The front end for the BAJer.

## Settings

Change the URL to the API server in [.env.development](.env.development) and [.env.production](.env.production)  accordingly before you run the development server or build the production target.

Needs [The BAJer-backend](https://gitlab.com/thebajer/bajer-backend) as API server.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
