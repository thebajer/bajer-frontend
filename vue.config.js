module.exports = {
  css: {
    sourceMap: true
  },
  devServer: {
    proxy: 'http://localhost:8000'
  }
}
