import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Preferences from './views/Preferences'
import PlotSubNav from './components/navigation/PlotSubNav'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: Home,
        subnav: PlotSubNav
      }
    },
    {
      path: '/preferences',
      name: 'preferences',
      components: {
        default: Preferences
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
