const getFetchDates = (state) => state.fetchDates
const getBibstem = (state) => state.bibstem
const getYearRange = (state) => [state.yearRange.minYear, state.yearRange.maxYear]

export default {
  getFetchDates,
  getBibstem,
  getYearRange
}
