import { expect } from 'chai'
import mutations from '@/store/plot/mutations'
import states from '@/store/plot/state'

// destructure assign `mutations`
const { ADD_PLOT, SET_RANGE } = mutations

// destructure assign `store`
const { state } = states

describe('mutations test', () => {
  it('ADD_PLOT test', () => {
    // apply mutation
    ADD_PLOT(state, 12)
    // assert result
    expect(state.title[12]).to.equal('Untitled 12')
  })
  it('SET_RANGE test', () => {
    // init plot
    ADD_PLOT(state, 12)
    // apply mutation
    SET_RANGE(state, { 'plotid': 12, 'from': 2010, 'until': 2018 })
    // assert result
    expect(state.range[12]).to.deep.equal({ 'from': 2010, 'until': 2018 })
  })
})
